package ictgradschool.web.lab12.ex1;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try(FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            boolean running = true;
            while(running) {
                System.out.println("Search for Article:");
                String article = "%" + Keyboard.readInput() + "%";


                try (PreparedStatement stmt = conn.prepareStatement("SELECT title, body FROM lab12_articles WHERE title LIKE ?;")) {


                    stmt.setString(1, article);


                    try (ResultSet r = stmt.executeQuery()) {


                        while (r.next()) {


                            System.out.println("Title: " + r.getString(1));
                            System.out.println("Article: " + r.getString(2));

                        }

                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }



    }
}
