package ictgradschool.web.lab12.ex3;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLDatabase implements Database {

    @Override
    public Connection getConnection() throws IOException, SQLException {


        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("mysql.properties")) {
            dbProps.load(fis);
        }

        Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps);
        return conn;
    }
}
