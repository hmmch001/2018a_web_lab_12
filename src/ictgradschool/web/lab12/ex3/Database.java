package ictgradschool.web.lab12.ex3;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface Database {

    Connection getConnection() throws IOException, SQLException;

}
