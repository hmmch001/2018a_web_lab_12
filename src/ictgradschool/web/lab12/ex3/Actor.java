package ictgradschool.web.lab12.ex3;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class Actor {

    private Integer actor_id;

    private String actor_fname;

    private String actor_lname;



    public Actor() {}

    public Actor( Integer actor_id, String actor_fname, String actor_lname) {
        this.actor_id = actor_id;
        this.actor_fname = actor_fname;
        this.actor_lname = actor_lname;

    }

    public Integer getActorId() {
        return actor_id;
    }

    public void setActorId(Integer actor_id) {
        this.actor_id = actor_id;
    }

    public String getFirstName() {
        return actor_fname;
    }

    public void setFirstName(String actor_fname) {
        this.actor_fname = actor_fname;
    }

    public String getLastName() {
        return actor_lname;
    }

    public void setLastName(String actor_lname) {
        this.actor_lname = actor_lname;
    }



    @Override
    public String toString() {
        return "";
    }
}
