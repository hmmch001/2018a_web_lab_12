package ictgradschool.web.lab12.ex3;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ActorDAO implements AutoCloseable {

    private final Database db;
    private final Connection conn;


    public ActorDAO(Database db) throws IOException, SQLException {
        this.db = db;
        this.conn = db.getConnection();
    }

    public Actor getActor(String input) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM pfilms_actor WHERE actor_fname = ? OR actor_lname = ?;")) {
            stmt.setString(1, input);
            stmt.setString(2, input);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return actorFromResultSet(rs);

                   } else {
                    return null;
                }
            }
        }

    }


    private Actor actorFromResultSet(ResultSet rs) throws SQLException {
        return new Actor(rs.getInt(1),rs.getString(2), rs.getString(3));
    }

    public List<String> getActorInfoById(int actorId) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT f.film_title, r.role_name FROM pfilms_participates_in p, pfilms_film f, pfilms_role r WHERE p.actor_id = ? AND p.film_id = f.film_id AND p.role_id = r.role_id;")) {
            stmt.setInt(1, actorId);
            try (ResultSet rs = stmt.executeQuery()) {
                List<String> actorInfo = new ArrayList<>();
                while (rs.next()) {
                    actorInfo.add(rs.getString(1) + " (" + rs.getString(2) + ")");
                }

                return actorInfo;
            }
        }
    }


    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
