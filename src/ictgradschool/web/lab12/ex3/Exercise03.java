package ictgradschool.web.lab12.ex3;

import com.mysql.jdbc.MySQLConnection;
import ictgradschool.web.lab12.Keyboard;
import ictgradschool.web.lab12.ex2.Article;
import ictgradschool.web.lab12.ex2.ArticleDAO;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class Exercise03 {


    public void start() throws IOException, SQLException {

            System.out.println("Welcome to the Film database!");
            mainMenu();




    }




    public void mainMenu() {
        System.out.println("Please select an option from the following: ");
        System.out.println("1. Information by Actor");
        System.out.println("2. Information by Movie");
        System.out.println("3. Information by Genre");
        System.out.println("4. Exit");
        String choice = Keyboard.readInput();
        if (choice.equals("1")) {
            getActorInfo();
        }else if(choice.equals("2")) {
            getMovieInfo();
        }else if (choice.equals("3")) {
            getGenreInfo();
        }
    }

    public void getActorInfo() throws IOException, SQLException{

        try (ActorDAO dao = new ActorDAO(new MySQLDatabase())){
            boolean running = true;
            while(running) {
                System.out.println("Please enter the name of the actor you wish to get information about, or press enter to return to the previous menu");
                String input =  Keyboard.readInput();
                System.out.println(input);
                if(input.equals("")){
                    mainMenu();
                    break;
                }
                Actor searchedActor = dao.getActor(input);
                if(searchedActor == null){
                    System.out.println("Sorry, we couldn't find any actor by that name.");
                    continue;
                }
                List<String> actorInfo = dao.getActorInfoById(searchedActor.getActorId());
                System.out.println(searchedActor.getFirstName() + " " + searchedActor.getLastName() + " is listed as being involved in the following films: ");
                for (String s: actorInfo) {
                    System.out.println(s);
                }
            }
        }
    }

    public static void main(String[] args)throws IOException, SQLException{
        Exercise03 eg = new Exercise03();
        eg.start();

    }
}
