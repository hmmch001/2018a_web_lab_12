package ictgradschool.web.lab12.ex2;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ArticleDAO implements AutoCloseable {

    private final Connection conn;

    public ArticleDAO(Connection conn) throws IOException, SQLException {

        this.conn = conn;
    }

    public List<Article> getArticles(String input) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT title, body FROM lab12_articles WHERE title LIKE ?;")) {
            stmt.setString(1, input);
            try (ResultSet rs = stmt.executeQuery()) {

                List<Article> articles = new ArrayList<>();
                while (rs.next()) {
                    articles.add(articleFromResultSet(rs));

                }
                return articles;
            }
        }
    }


    private Article articleFromResultSet(ResultSet rs) throws SQLException {
         return new Article(rs.getString(1), rs.getString(2));
    }


    @Override
    public void close() throws SQLException {
        this.conn.close();
    }

}

