package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) throws IOException, SQLException {
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {

            try (ArticleDAO dao = new ArticleDAO(conn)) {
                boolean running = true;
                while (running) {
                    System.out.println("Search for Article:");
                    String article = "%" + Keyboard.readInput() + "%";
                    List<Article> articles = dao.getArticles(article);
                    for (Article a : articles) {
                        System.out.println(a);
                    }


                }
            }
        }
    }

}

